//
//  CoCoImageContainerConfig.h
//  CoCoImageContainer
//
//  Created by 陈明 on 2017/6/13.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGBase.h>
#import <UIKit/UIImage.h>


typedef enum : NSUInteger {
    CoCoImageContainerConfigPlusSizeTypeScale,
    CoCoImageContainerConfigPlusSizeTypeSize,
} CoCoImageContainerConfigPlusSizeType;

@interface CoCoImageContainerConfig : NSObject

@property (nonatomic, assign) CGFloat topSpec;
@property (nonatomic, assign) CGFloat bottomSpec;
@property (nonatomic, assign) CGFloat leftSpec;
@property (nonatomic, assign) CGFloat rightSpec;
@property (nonatomic, assign) CGFloat columnCount;
@property (nonatomic, assign) CGFloat itemSpec;
@property (nonatomic, assign) CGFloat viewWidth;
@property (nonatomic, assign) NSInteger maxCount;


/**
 * Plus Setting
 */

@property (nonatomic, strong) UIImage *plusImage;
@property (nonatomic, assign) CGPoint plusCenterRatio;  // 中心位置比例，默认为0.5 , 0.5

/**
 * 默认为CoCoImageContainerConfigPlusSizeTypeScale，表示与superView的比例
 */
@property (nonatomic, assign) CoCoImageContainerConfigPlusSizeType plusSizeType;


// 当type为 CoCoImageContainerConfigPlusSizeTypeScale 时，以下设置生效
@property (nonatomic, assign) CGFloat plusScale; // 与superView的比例，默认为1

// 当type为 CoCoImageContainerConfigPlusSizeTypeSize 时，以下设置生效
@property (nonatomic, assign) CGSize plusSize; // 默认为 100,100


+ (CoCoImageContainerConfig *)defaultConfig;


@end
