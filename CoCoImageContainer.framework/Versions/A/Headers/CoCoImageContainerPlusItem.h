//
//  CoCoImageContainerPlusItem.h
//  CoCoImageContainer
//
//  Created by 陈明 on 2017/6/17.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoImageContainerPlusItem : NSObject
@property (nonatomic, assign) BOOL removeable;
@end
