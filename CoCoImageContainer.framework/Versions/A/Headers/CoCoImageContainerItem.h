//
//  CoCoImageContainerItem.h
//  CoCoImageContainer
//
//  Created by 陈明 on 2017/6/13.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGBase.h>

@class PHAsset;

@interface CoCoImageContainerItem : NSObject
@property (nonatomic, strong) PHAsset *phAsset;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) id object;
@property (nonatomic, assign) BOOL removeable; // 是否可以删除，是则显示删除按钮，不是则隐藏
@property (nonatomic, assign) BOOL hidden;
@property (nonatomic, assign) BOOL showProgressView;
@property (nonatomic, assign) CGFloat progress;
@end
