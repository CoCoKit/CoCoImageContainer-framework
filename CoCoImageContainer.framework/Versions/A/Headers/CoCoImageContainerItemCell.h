//
//  CoCoImageContainerItemCell.h
//  CoCoImageContainer
//
//  Created by 陈明 on 2017/6/13.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CoCoImageContainerItemCell, CoCoImageContainerItem;

@protocol CoCoImageContainerItemCellDelegate <NSObject>

- (void)containerItemDeleteActionWithCell:(CoCoImageContainerItemCell *)cell;
- (void)containerItemAddActionWithCell:(CoCoImageContainerItemCell *)cell;

@end

@interface CoCoImageContainerItemCell : UICollectionViewCell

@property (nonatomic, weak) id<CoCoImageContainerItemCellDelegate> delegate;

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) NSString *identifier;


- (void)updateData:(id)object config:(id)config;
@end
