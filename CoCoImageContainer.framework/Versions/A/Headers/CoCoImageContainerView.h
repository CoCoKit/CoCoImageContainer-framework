//
//  CoCoImageContainerView.h
//  CoCoImageContainer
//
//  Created by 陈明 on 2017/6/13.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoCoImageContainerConfig.h"
#import "CoCoImageContainerItem.h"


typedef enum : NSUInteger {
    CoCoImageContainerViewErrorTypeBeyoundMaxCount,
} CoCoImageContainerViewErrorType;

@class CoCoImageContainerView;

@protocol CoCoImageContainerViewDelegate <NSObject>
@optional
- (void)imageContainerView:(CoCoImageContainerView *)containerView heightDidChange:(CGFloat)height;
- (void)imageContainerViewDidTapAddPhoto:(CoCoImageContainerView *)containerView;
- (void)imageContainerView:(CoCoImageContainerView *)containerView errorMessageType:(CoCoImageContainerViewErrorType)type;
@end

@interface CoCoImageContainerView : UIView

@property (nonatomic, weak) id<CoCoImageContainerViewDelegate> delegate;

- (instancetype)initWithConfig:(CoCoImageContainerConfig *)config;

- (instancetype)init __attribute__((unavailable("禁止使用init方法，请使用头文件自定义方法")));

- (NSArray <CoCoImageContainerItem *> *)allSource;

- (void)addItem:(CoCoImageContainerItem *)item;
- (void)addItems:(NSArray <CoCoImageContainerItem *> *)items;
- (void)refreshView;


- (void)deleteAllItem;

- (void)setEditble:(BOOL)able;

@end
